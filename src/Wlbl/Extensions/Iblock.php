<?php
namespace Wlbl\Extensions;

use Bitrix\Main\Loader;

class Iblock
{
	private static $aliases = [];
	private static $ids = [];

	/**
	 * Устанавливаем синонимы для кодов инфоблоков
	 * @param array $aliases
	 */
	public static function setAliases($aliases = [])
	{
		if (!is_array($aliases)) {
			self::$aliases = $aliases;
		}
	}

	/**
	 * Возвращается id инфоблока по его коду, дополнительно проверяет наличие синонимов кодов в $aliases
	 * @param string $iblockAlias код инфоблока
	 * @return int id инфоблока
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ArgumentNullException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function getIdByAlias($iblockAlias, $SID = SITE_ID)
	{
		$code = $iblockAlias;

		if (empty($iblockAlias)) {
			throw new \Bitrix\Main\ArgumentNullException('iblockAlias');
		}

		if (!empty(self::$aliases[$code])) {
			$code = self::$aliases[$code];
		}

		if (!empty(self::$ids[$code])) {
			return self::$ids[$code];
		}

		Loader::includeModule('iblock');

		$obRes = \CIBlock::GetList(
			[], 
			[ 
				'SITE_ID' => $SID, 
				'CODE' => $iblockAlias,
				'ACTIVE' => 'Y'
			]
		);
		$arRes = $obRes->Fetch();

		$id = intval($arRes['ID']);

		if ($id > 0) {
			self::$ids[$code] = $id;
			return $id;
		}

		return -1;
	}
}