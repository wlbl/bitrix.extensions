<?php

namespace Wlbl\Extensions;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;

class HighloadBlock
{
	/**
	 * Получение класса для работы с HighloadBlock'ом  по имени.
	 * @param $name
	 * @return \Bitrix\Main\Entity\DataManager
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function getClassByName($name)
	{
		Loader::includeModule('highloadblock');

		$hl = HighloadBlockTable::getRow([
			'filter' => ['NAME' => $name],
			'select' => ['ID']
		]);

		return self::getClassById($hl['ID']);
	}

	/**
	 * Получение класса для работы с HighloadBlock'ом  по id;
	 * @param $id
	 * @return \Bitrix\Main\Entity\DataManager
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getClassById($id)
	{
		Loader::includeModule('highloadblock');

		$hlBlock = HighloadBlockTable::getById($id)->fetch();
		$entity = HighloadBlockTable::compileEntity($hlBlock);

		return $entity->getDataClass();
	}
}