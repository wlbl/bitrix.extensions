<?php

namespace Wlbl\Extensions;

class Debug
{
	public static function dump($object, $die = false, $restartBuffer = false)
	{
		if ($restartBuffer) {
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
		}

		echo '<pre>';

		if (is_null($object)) {
			var_export('NULL');
		} elseif (empty($object)) {
			var_export('EMPTY');
		} else {
			var_export($object);
		}

		echo '</pre>';

		if ($die) {
			die();
		}
	}

	public static function consoleLog($object)
	{
		echo '<script>console.log(' . \CUtil::PhpToJSObject($object) . ')</script>';
	}
}
