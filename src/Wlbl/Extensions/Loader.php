<?php

namespace Wlbl\Extensions;

use Bitrix\Main;
use Bitrix\Main\LoaderException;

class Loader
{
	public static function includeModules($modules = [])
	{
		if (gettype($modules) == 'string') {
			$modules = [$modules];
		}

		foreach ($modules as $module) {
			if (!Main\Loader::includeModule($module)) {
				throw new LoaderException('Модуль ' . $module . ' не установлен', 1);
			}
		}

		return true;
	}
}